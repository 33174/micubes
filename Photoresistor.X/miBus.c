/*
 * MiBus Communication Protocol - The communication protocol formerly known as Nathan
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include "miBus.h"

void initMiBus() {
    TRISCbits.TRISC0 = 1; // Clock Input
    ANSELCbits.ANSC0 = 0; // Digital Pin
    TRISCbits.TRISC1 = 1; // Data Input
    ANSELCbits.ANSC1 = 0; // Digital Pin
    TRISCbits.TRISC2 = 0; // Data Output
    ANSELCbits.ANSC2 = 0; // Digital Pin
    TRISAbits.TRISA1 = 1; // Clock Mirror
    ANSELAbits.ANSA1 = 0; // Digital Pin
}

unsigned char readMiBus() {
    unsigned char i = 0;
    unsigned char data = 0b00000000;

    // Ignore zero data
    do {
        // Wait for empty
        i = 0;
        while (i < 16) {
            WAITHIGH;
            WAITLOW;
            __delay_us(10);
            if (DATI == 0) {
                i++;
            } else {
                i = 0;
            }
        }

        // Read Data
        for (i = 0; i < 8; i++) {
            WAITHIGH; // Wait while clock High
            WAITLOW;
            __delay_us(10);
            if (DATI == 1) {
                data |= BITR(i);
            }
        }

    } while (data == 0b00000000);

    return data;
}

void writeMiBus(unsigned char data) {
    unsigned char i = 0;

    for (i = 0; i < 16; i++) {
        WAITHIGH;
        __delay_us(10);
        DATO = 0;
        WAITLOW;
    }

    for (i = 0; i < 8; i++) {
        WAITHIGH;
        __delay_us(10);
        DATO = BITW(data, i);
        WAITLOW;
    }

    for (i = 0; i < 2; i++) {
        WAITHIGH;
        __delay_us(10);
        DATO = 1;
        WAITLOW;
    }
}
