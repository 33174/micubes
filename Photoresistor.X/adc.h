/* 
 * PHOTORESISTOR ADC
 */

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// Setup ADC
void initADC();

// Read Value from ADC
unsigned char readADC();
