/*
 * MICROPHONE ADC
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include "adc.h"

void initADC() {
    TRISAbits.TRISA4 = 1; // A4 Input
    ANSELAbits.ANSA4 = 1; // A4 Analogue

    ADCON0 = 0b00001100;
    ADCON1 = 0b10100000;
}

unsigned char readADC() {
    unsigned int value = 0;

    ADCON0bits.ADON=1;  // ADC ON
    ADCON0bits.GO_nDONE=1;  // Start Conversion
    while(ADCON0bits.GO_nDONE); // Wait
    ADCON0bits.ADON=0;  // ADC OFF

    value = ADRES;

    return (value >> 2);
}