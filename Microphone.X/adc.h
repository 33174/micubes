/*
 * MICROPHONE ADC
 */

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

void initADC();

unsigned char readADC();
