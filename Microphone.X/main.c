/*
 * MICROPHONE BLOCK
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include <stdio.h>
#include <stdlib.h>

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// Chip Frequency
#ifndef FREQ
#define _XTAL_FREQ 16000000
#endif

// Options
#pragma config WDTE=OFF, MCLRE=OFF, CP=OFF, FOSC=INTOSC

// Functions
#include "miBus.h"
#include "adc.h"

#define MICMIN 30

// Mode
#define MODE PORTAbits.RA5

int main() {

    // Clock Setup
    OSCCONbits.IRCF = 0b1111; //16MHz
    __delay_us(10);

    // MiBus
    initMiBus();

    // ADC
    initADC();

    // Modes Select Pin
    TRISAbits.TRISA5 = 1;

    // Data Variable
    unsigned char data = 0;
    unsigned char value = 0;

    while(1) {

        data = readMiBus();

        // Read ADC
        value = readADC();

        // Mode 0 - Scale Data
        if (MODE == 0) {
            // Scale value between data range
            data = (((value - MICMIN) * (data - 1)) / (255 - MICMIN)) + 1;
            
        // Mode 1 - Sound Trigger
        } else {
            if (value >= data) {
                data = 0b11111111;
            } else {
                data = 0b00000001;
            }
        }

        writeMiBus(data);
    }
}

