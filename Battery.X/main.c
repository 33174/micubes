/* 
 * BATTERY BLOCK
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <pic10f220.h>

// CONFIG
#pragma config IOSCFS = 4MHZ    // Internal Oscillator Frequency Select bit (4 MHz)
#pragma config MCPU = ON        // Master Clear Pull-up Enable bit (Pull-up enabled)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config CP = OFF         // Code protection bit (Code protection off)
#pragma config MCLRE = OFF      // GP3/MCLR Pin Function Select bit (GP3/MCLR pin function is digital I/O, MCLR internally tied to VDD)
#define _XTAL_FREQ 4000000      // For delays

// Pulse width
#define PULSE 50

// Full = 210, Empty = 140
#define LOWBATT  160

// GP0 - Battery Monitor (ADC + LED)
// GP2 - Clock (OUTPUT)
// GP1 - Data (OUTPUT)
// GP3 - Button (INPUT)
#define LED GP3
#define DAT GP1
#define CLK GP2
#define BTN GP3

void pulse(unsigned char number) {
    unsigned char i = 0;
    for (i = 0; i < number; i++) {
        CLK = 1;
        __delay_us(PULSE);
        CLK = 0;
        __delay_us(PULSE);
    }
}

int main() {

    // Pin I/O definition
    TRISGPIO = 0b00001001;

    // Configeration
    ADCON0 = 0b01000001;
    OPTION = 0b11000000;
    FOSC4 = 0;

    CLK = 1;
    DAT = 1;
    
    __delay_us(10); // Special Delay
    
    while(1) {

        // If Button ON Transmit '1'
        if (BTN == 1) {
            
            // Empty
            DAT = 0;
            pulse(16);
            
            // Data + Tail
            DAT = 1; 
            pulse(10);

        // If Button OFF Transmit '0'
        } else {

            // Empty
            DAT = 0;
            pulse(16);

            // Data
            DAT = 1;
            pulse(1);
            DAT = 0;
            pulse(7);

            // Tail
            DAT = 1;
            pulse(2);
            
        }

        // LOW BATTERY CHECK
        TRISGPIO = 0b00001001; // GP0 is input
        ADCON0 = 0b01000011; // ADC On, Start conversion
        while(ADCON0bits.nDONE); // Wait for ADC (~4uS)
        if(ADRES < LOWBATT){
            ADCON0 = 0b00000001; // ADC Off
            TRISGPIO = 0b00001000; // GP0 is output
            LED = 0; // LED on
        } else {
            LED = 1; // LED off
        }
    }
}

