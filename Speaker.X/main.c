/*
 * SPEAKER BLOCK
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include <stdio.h>
#include <stdlib.h>

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// Chip Frequency
#ifndef FREQ
#define _XTAL_FREQ 16000000
#endif

// Options
#pragma config WDTE=OFF, MCLRE=OFF, CP=OFF, FOSC=INTOSC

// Functions
#include "miBus.h"
#include "pwm.h"

// Mode
#define MODE PORTAbits.RA5

int main() {

    // Clock Setup
    OSCCONbits.IRCF = 0b1111; //16MHz
    __delay_us(10);

    // MiBus
    initMiBus();

    // Speaker PWM
    initT2();
    initPWM_T();
    initPWM_V();

    // Modes Select Pin
    TRISAbits.TRISA5 = 1;

    // Data Variable
    unsigned char data = 0;
    

    while(1) {

        data = readMiBus();

        // Mode 0 - Tone
        if (MODE == 0) {
            if (data == 0b00000001) {
                PR2 = 1;
            } else {
                data = 256 - data;
                PR2 = (((data - 1) * (MAXTONE - MINTONE)) / (255 - 1)) + MINTONE;
            }

        // Mode 1 - Volume (Not very good)
        } else {
            VOL = 255 - data;
        }

        writeMiBus(data);

    }
}
