/* 
 * SPEAKER PWM
 */

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// Speaker Range
#define MAXTONE 255
#define MINTONE 130

#define VOL PWM2DCH

// Setup Timer 2
void initT2();

// Tone PWM
void initPWM_T();

// Volume PWM
void initPWM_V();