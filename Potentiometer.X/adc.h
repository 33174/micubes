/* 
 * POTENTIOMETER ADC
 */

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// initialise ADC
void initADC();

// Read ADC Value
unsigned char readADC();
