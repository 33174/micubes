/*
 * RGB PWM
 * v1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include "pwm.h"

void initT2() {
    PR2 = 0b11111001;
    T2CON = 0b00000100;
}

void initPWM_R() {
    // PWM1 on C5
    TRISCbits.TRISC5 = 0;
    PWM1CONbits.PWM1EN = 1; // PWM1 Enabled
    PWM1CONbits.PWM1OE = 1; //PWM1 Pin Enabled
    PWM1CONbits.PWM1OUT = 0; // Output Value
    PWM1CONbits.PWM1POL = 0; // Active High
}

void initPWM_G() {
    // PWM3 on A2
    TRISAbits.TRISA2 = 0;
    PWM3CONbits.PWM3EN = 1; // PWM1 Enabled
    PWM3CONbits.PWM3OE = 1; //PWM1 Pin Enabled
    PWM3CONbits.PWM3OUT = 0; // Output Value
    PWM3CONbits.PWM3POL = 0; // Active High
}

void initPWM_B() {
    // PWM2 on C3
    TRISCbits.TRISC3 = 0;
    PWM2CONbits.PWM2EN = 1; // PWM1 Enabled
    PWM2CONbits.PWM2OE = 1; //PWM1 Pin Enabled
    PWM2CONbits.PWM2OUT = 0; // Output Value
    PWM2CONbits.PWM2POL = 0; // Active High
}
