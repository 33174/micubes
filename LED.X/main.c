/*
 * RGB LED BLOCK
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */

#include <stdio.h>
#include <stdlib.h>

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// Chip Frequency
#ifndef FREQ
#define _XTAL_FREQ 16000000
#endif

// Options
#pragma config WDTE=OFF, MCLRE=ON, CP=OFF, FOSC=INTOSC

// Functions
#include "miBus.h"
#include "pwm.h"
#include "rgb.h"

// Mode
#define MODE PORTAbits.RA5

int main() {
    
    // Clock Setup
    OSCCONbits.IRCF = 0b1111; //16MHz
    __delay_us(10);

    // MiBus
    initMiBus();

    // RGB PWM
    initT2();
    initPWM_R();
    initPWM_G();
    initPWM_B();

    // Modes Select Pin
    TRISAbits.TRISA5 = 1;

    // Data Variable
    unsigned char data = 0;
    
    while(1) {

        data = readMiBus();

        // Mode 0 - Rainbow
        if (MODE == 0) {
            PWM_R = redVal[data-1]; // Red
            PWM_G = greenVal[data-1]; // Green
            PWM_B = blueVal[data-1]; // Blue

        // Mode 1 - White Fade
        } else {
            PWM_R = 255-data; // Red
            PWM_G = 255-data; // Green
            PWM_B = 255-data; // Blue
        }

        writeMiBus(data);
    }
}

