/*
 * RGB PWM
 */

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// PWM Value Registers
#define PWM_R PWM1DCH
#define PWM_G PWM3DCH
#define PWM_B PWM2DCH

// Setup Timer 2
void initT2();

// Red LED PWM
void initPWM_R();

// Green LED PWM
void initPWM_G();

// Blue LED PWM
void initPWM_B();
