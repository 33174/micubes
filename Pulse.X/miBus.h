/* 
 * MiBus Communication Protocol - The communication protocol formerly known as Nathan
 */

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

#ifndef FREQ
#define _XTAL_FREQ 16000000
#endif

// Pins
#define CLK PORTCbits.RC0
#define DATI PORTCbits.RC1
#define DATO PORTCbits.RC2

// Waits
#define WAITLOW while (CLK == 0)
#define WAITHIGH while (CLK == 1)

// Bit operations
#define BITW(x,n) (((x) >> (n)) & 1)
#define BITR(n) (1 << (n))

// Initialise MiBus
void initMiBus();

// Read Data from MiBus
unsigned char readMiBus();

// Write Data to MiBus
void writeMiBus(unsigned char data);
