/*
 * PULSE BLOCK
 * V1.0
 * 20/10/2013
 * Nathan Macfarlane
 */


#include <stdio.h>
#include <stdlib.h>

#ifndef PIC
#include <xc.h>
#include <pic16f1503.h>
#endif

// Chip Frequency
#ifndef FREQ
#define _XTAL_FREQ 16000000
#endif

// Options
#pragma config WDTE=OFF, MCLRE=OFF, CP=OFF, FOSC=INTOSC

// Functions
#include "miBus.h"
#include "adc.h"

// Mode
#define MODE PORTAbits.RA5

int main() {
    
    // Clock Setup
    OSCCONbits.IRCF = 0b1111; //16MHz
    __delay_us(10);

    // MiBus
    initMiBus();

    // ADC
    initADC();

    // Modes Select Pin
    TRISAbits.TRISA5 = 1;

    // Data Variable
    unsigned char data = 0;
    unsigned char value = 0;
    unsigned char i = 0;
    unsigned char j = 0;

    while(1) {

        data = readMiBus();

        // Mode 0 - Pulse
        if (MODE == 0) {

            if (data != 0b00000001) {
                value = 258 - readADC();
                // Variable Delay
                for (i = 0; i < value; i++) {
                    // Zero Data
                    writeMiBus(0b11111111);
                    __delay_ms(6);
                }
                value = 258 - readADC();
                // Variable Delay
                for (i = 0; i < value; i++) {
                    // Once Data
                    writeMiBus(0b00000001);
                    __delay_ms(6);
                }
            // Off State
            } else {
                writeMiBus(data);
            }

        // Mode 1 - Sweep
        } else {

            if (data != 0b00000001) {
                // Sweep Up
                for (i = 2; i < 255; i++) {
                    writeMiBus(i);
                    writeMiBus(i);
                    writeMiBus(i);
                    value = 255 - readADC();
                    // Variable Delay
                    for (j = 0; j < value; j++) {
                        __delay_us(200);
                    }
                }
                // Sweep Down
                for (i = 255; i > 2; i--) {
                    writeMiBus(i);
                    writeMiBus(i);
                    writeMiBus(i);
                    value = 255 - readADC();
                    // Variable Delay
                    for (j = 0; j < value; j++) {
                        __delay_us(200);
                    }
                }
            // Off State
            } else {
                writeMiBus(data);
            }
        }
    }
}

